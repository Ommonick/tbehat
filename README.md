###BEHAT testing

Данный репозиторий содержит примеры использования Behat для автоматизации некоторых действий при разработке и тестировании

Запуск тестов по tender.pro
 - **bin/behat --tags tender** 



чтобы запустить тесты, нужно:
 
 - сделать клон репозитория
 - **composer update** из папки проекта
 - **bin/behat --init** для генерации базового featureContext
 
 - **bin/behat** для запуска всех тестов
 - **bin/behat --tags %tag_name%** для запуска тестов по тэгу tag_name
 - **bin/behat --help** отображает справку по Behat




Примечание: некоторые тесты содержат выполнение консольных команд. (skyengNotifier например) в отдельном проекте Behat они работать не будут.
Для работы этих тестов необходимо в composer.json рабочего проекта добавить параметры из composer.json этого репозитория, 
сделать composer update и выполнить bin/behat --init в папке рабочего проекта. 
После этого Behat создаст feature директорию (для сценариев) и bootstrap директорию (для контекста). 
Необходимо будет перенести туда сценарии и контекст с этого репозитория.  


###Tag filters supports three logical operators:

Tags separated by commas will be combined using the OR operation

Tags separated by && will be combined using the AND operation

Tags preceded by ~ will be excluded using the NOT operation

####С указанными через запятую тэгами - запускаются с любым из названий (и с обоими тэгами тоже)
- --tags '@**orm**,@**database**'
- --tags 'ticket,723'

####С двумя символами && запускаются только с указанными обоими тэгами
- --tags '@**orm**&&@**fixtures**'

НО если тэги указаны **без ковычек** то запуск идет сценария с любым тэгом

- --tags first&&second запустит как сценарии с обоими тэгами так и с указанными по отдельности

####С символом ~ запускаются все кроме сценариев с данным тэгом
- --tags '~@**javascript**'


####С параметром --name 'text' запускаются все сценарии, содержащие в описании 'text'
- --name 'number has'



