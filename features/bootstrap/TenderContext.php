<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use TenderApi as Api;


/**
 * Defines application features from the specific context.
 */
class TenderContext implements Context
{
    public $_response;
    public $client;
    private $sessionData;

    /**
     * Initializes context.
     * Every scenario gets it's own context object.
     */
    public function __construct()
    {
        $this->client = new Api;
        $this->sessionData = [
            "tender_id" => '',
            "company_id" => '',
            "company_name" => ''
        ];

    }

    /**
     * @Given tender lists performed
     */
    public function tenderListsPerform()
    {
        //Запрос tender.list
        $options = ["_key" => "1732ede4de680a0c93d81f01d7bac7d1",
            "set_type_id" => "2",
            "set_id" => "7964",
            "max_rows" => "3",
            "open_only" => "t"];
        $this->_response = $this->client->performGetRequest("_info.tenderlist_by_set.json", $options);

        //вытаскиваем данные для последующих проверок
        $data = json_decode($this->_response->getBody(), true);
        $this->sessionData["tender_id"] = $data['result']['data'][0]['id'];
        $this->sessionData["company_id"] = $data['result']['data'][0]['company_id'];
        $this->sessionData["company_name"] = $data['result']['data'][0]['company_name'];
    }

    /**
     * @Given tender info performed
     */
    public function tenderInfoPerform()
    {
        //Запрос tenderInfo
        $options = [
            "_key" => "1732ede4de680a0c93d81f01d7bac7d1",
            "company_id" => $this->sessionData["company_id"],
            "id" => $this->sessionData["tender_id"]
        ];
        $this->_response = $this->client->performGetRequest("_tender.info.json", $options);
    }

    /**
     * @Given company info performed
     */
    public function companyInfoPerform()
    {
        $options = [
            "id" => $this->sessionData["company_id"]
        ];
        $this->_response = $this->client->performGetRequest("_company.info_public.json", $options);
    }



    /**
     * @Given get :url with:
     */
    public
    function getWithOptions($url, PyStringNode $queryArray)
    {
        $options = json_decode($queryArray, true);
        $this->_response = $this->client->performGetRequest($url, $options);

    }

    /**
     * @Given post :uri with:
     */
    public
    function postRequestWithQueryArray($uri, PyStringNode $queryArray)
    {
        $options = json_decode($queryArray, true);

        $this->_response = $this->client->performPostRequest($uri, $options);
    }



    /**
     * @Then check response :field equal session data :key
     */
    public
    function responseFieldEqualStoredData($field, $key)
    {
        $dot = new \Adbar\Dot(json_decode($this->_response->getBody()->__toString(), true));
        $value = $dot->get($field);

        $text = $this->sessionData[$key];

        if (preg_match("/$text/i", $value) < 1) {
            throw new \Exception('Not found text: ' . $text . '');
        }
    }


    /**
     * @Then check response :field exists
     */
    public
    function responseFieldExists($field)
    {
        $dot = new \Adbar\Dot(json_decode($this->_response->getBody()->__toString(), true));
        $value = $dot->get($field);
        if (!isset($value)) {
            throw new Exception("field unexists\n");
        }
    }

    /**
     * @Then check response :field not exists
     */
    public
    function responseFieldNotExists($field)
    {
        $dot = new \Adbar\Dot(json_decode($this->_response->getBody()->__toString(), true));
        $value = $dot->get($field);
        if (isset($value)) {
            throw new Exception("field exists\n");
        }
    }

    /**
     * @Then check response :field number > :num
     */
    public
    function responseNumMoreThan($field, $num)
    {
        $dot = new \Adbar\Dot(json_decode($this->_response->getBody()->__toString(), true));
        $value = $dot->get($field);
        if (intval($value) < intval($num)) {
            throw new Exception("value string is empty($value)\n");
        }
    }

    /**
     * @Then check response :field length > :num
     */
    public
    function responseValueRequired($field, $num)
    {
        $dot = new \Adbar\Dot(json_decode($this->_response->getBody()->__toString(), true));
        $value = $dot->get($field);
        if (strlen($value) <= intval($num)) {
            throw new Exception("value string is less than $num ($value)\n");
        }
    }

    /**
     * @Then check response :fieldA count equals to response field :fieldB
     */
    public
    function responseItemsFieldEqualToCountField($fieldA, $fieldB)
    {
        $dot = new \Adbar\Dot(json_decode($this->_response->getBody()->__toString(), true));
        $array = $dot->get($fieldA);
        $value = $dot->get($fieldB);

        if (count($array) != intval($value)) {
            throw new Exception("Count mismatch, $value expected \n");
        }
    }


    /**
     * @Then check response :fieldA equals :fieldB
     */
    public
    function responseTwoFieldsAreEqual($fieldA, $fieldB)
    {
        $dot = new \Adbar\Dot(json_decode($this->_response->getBody()->__toString(), true));
        $value1 = $dot->get($fieldA);
        $value2 = $dot->get($fieldB);

        if ($value1 != $value2) {
            throw new Exception("$value1 Not euqal $value2 \n");
        }
    }

    /**
     * @Then check response :fieldA count equals to :num
     */
    public
    function responseItemsFieldEqualToNum($fieldA, $num)
    {
        $dot = new \Adbar\Dot(json_decode($this->_response->getBody()->__toString(), true));
        $array = $dot->get($fieldA);

        if (count($array) != intval($num)) {
            throw new Exception("Count mismatch, $num expected \n");
        }
    }

    /**
     * @Then check response :field elements exist
     */
    public
    function responseFieldElementsExist($field)
    {
        $dot = new \Adbar\Dot(json_decode($this->_response->getBody()->__toString(), true));
        $value = $dot->get($field);
        if (count($value) < 1) {
            throw new Exception("empty array of  $field\n");
        }
    }

    /**
     * @Then check response :field has :text
     */
    public
    function responseFieldElementHasText($field, $text)
    {
        $dot = new \Adbar\Dot(json_decode($this->_response->getBody()->__toString(), true));
        $value = $dot->get($field);

        if (preg_match("/$text/i", $value) < 1) {
            throw new \Exception('Not found text: ' . $text . '');
        }
    }

    /**
     * @Then display response :field
     */
    public
    function displayField($field)
    {
        $dot = new \Adbar\Dot(json_decode($this->_response->getBody()->__toString(), true));
        $value = $dot->get($field);

        var_dump(count($value));


    }

    /**
     * @Given show response
     */
    public
    function showResponse()
    {
        var_dump(json_decode($this->_response->getBody()->__toString(), true));
    }


    /**
     * @Then the response uses JSON format
     */
    public
    function responseUseJsonFormat()
    {
        $data = json_decode($this->_response->getBody(true));
        if (empty($data)) {
            throw new Exception("Response is not JSON\n" . $this->_response);
        }
    }


    /**
     * @Given status code must be :code
     */
    public
    function statusCodeCheck($code)
    {
        if (!($code == $this->_response->getStatusCode())) {
            throw new \Exception('Actual code: ' . $this->_response->getStatusCode() . ' expected: ' . $code);
        }
    }

    /**
     * @Given response contains :message
     *
     */
    public
    function messageCheck($message)
    {
        $text = $this->_response->getBody()->__toString();
        if (preg_match("/$message/i", $text) < 1) {
            throw new \Exception('Not found text: ' . $message . '');
        }
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}